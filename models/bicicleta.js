var Bicicleta = function(id, color, modelo, ubicacion) {
	this.id = id;
	this.color = color;
	this.modelo = modelo;
	this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
	return 'id: ' + this.id + " | color: " + this.color;
};

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
	Bicicleta.allBicis.push(aBici)
}

Bicicleta.findById = function(aBiciId){
	var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
	if (aBici)
		return aBici;
	else
		throw new Error (`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.updateById = function(aBiciId, bici){
	var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
	if (aBici){
		var newBici = {};
		console.log(bici.id)
		if(bici.id) newBici.id = bici.id;
		if(bici.color) newBici.color = bici.color;
		if(bici.modelo) newBici.modelo = bici.modelo;
		if(bici.ubicacion[0]) newBici.ubicacion[0] = bici.ubicacion[0];
		if(bici.ubicacion[1]) newBici.ubicacion[1] = bici.ubicacion[1];
		Object.assign(aBici, newBici);
		for(var i = 0; i < Bicicleta.allBicis.length; i++){
			if(Bicicleta.allBicis[i].id == aBiciId){
				Bicicleta.allBicis.splice(i,1);
				break;
			}
		}
		Bicicleta.add(aBici)
		return aBici;
	}
	else
		throw new Error (`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
	//var aBici = Bicicleta.findById(aBiciId);
	for(var i = 0; i < Bicicleta.allBicis.length; i++){
		if(Bicicleta.allBicis[i].id == aBiciId){
			Bicicleta.allBicis.splice(i,1);
			break;
		}
	}
}

var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932,-58.3808287]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta